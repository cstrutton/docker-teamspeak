# -----------------------------------------------------------------------------
# docker-teamspeak
#
# Builds a basic docker image that can run TeamSpeak
# (http://teamspeak.com/).
#
# Authors: Isaac Bythewood
# Updated: May 18th, 2015
# Require: Docker (http://www.docker.io/)
# -----------------------------------------------------------------------------

# Base system is the LTS version of Ubuntu.
#FROM   ubuntu:14.04
FROM debian:jessie

ENV    TS_VERSION 3.0.11.3

RUN    DEBIAN_FRONTEND=noninteractive \
       apt-get -y update && \
       apt-get -y upgrade && \
       apt-get install --no-install-recommends -y \
       curl


# Download and install TeamSpeak 3
RUN    curl "http://dl.4players.de/ts/releases/$TS_VERSION/teamspeak3-server_linux-amd64-$TS_VERSION.tar.gz" -o teamspeak3-server_linux-amd64-$TS_VERSION.tar.gz
RUN    tar zxf teamspeak3-server_linux-amd64-$TS_VERSION.tar.gz; mv teamspeak3-server_linux-amd64 /opt/teamspeak; rm teamspeak3-server_linux-amd64-$TS_VERSION.tar.gz

# Load in all of our config files.
ADD    ./scripts/start /start

# Fix all permissions
RUN    chmod +x /start

# /start runs it.
EXPOSE 9987/udp
EXPOSE 10011
EXPOSE 30033

VOLUME ["/data"]
CMD    ["/start"]
